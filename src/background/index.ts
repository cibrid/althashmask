import AlthashController from './controllers';

// Add instance to window for debugging
const controller = new AlthashController();
Object.assign(window, { controller });
