import axios from 'axios';

import AlthashController from '.';
import IController from './iController';
import { MESSAGE_TYPE } from '../../constants';

const INIT_VALUES = {
  getPriceInterval: undefined,
  htmlPriceUSD: 0,
};

export default class ExternalController extends IController {
  private static GET_PRICE_INTERVAL_MS: number = 60000;

  private getPriceInterval?: number = INIT_VALUES.getPriceInterval;
  private htmlPriceUSD: number = INIT_VALUES.htmlPriceUSD;

  constructor(main: AlthashController) {
    super('external', main);
    this.initFinished();
  }

  public calculateHtmlToUSD = (balance: number): number => {
    return this.htmlPriceUSD ? Number((this.htmlPriceUSD * balance).toFixed(2)) : 0;
  }

  /*
  * Starts polling for periodic info updates.
  */
  public startPolling = async () => {
    await this.getHtmlPrice();
    if (!this.getPriceInterval) {
      this.getPriceInterval = window.setInterval(() => {
        this.getHtmlPrice();
      }, ExternalController.GET_PRICE_INTERVAL_MS);
    }
  }

  /*
  * Stops polling for the periodic info updates.
  */
  public stopPolling = () => {
    if (this.getPriceInterval) {
      clearInterval(this.getPriceInterval);
      this.getPriceInterval = undefined;
    }
  }

  /*
  * Gets the current Html market price.
  */
  private getHtmlPrice = async () => {
    try {
      const jsonObj = await axios.get('https://api.coinmarketcap.com/v2/ticker/1684/');
      this.htmlPriceUSD = jsonObj.data.data.quotes.USD.price;

      if (this.main.account.loggedInAccount
        && this.main.account.loggedInAccount.wallet
        && this.main.account.loggedInAccount.wallet.info
      ) {
        const htmlUSD = this.calculateHtmlToUSD(this.main.account.loggedInAccount.wallet.info.balance);
        this.main.account.loggedInAccount.wallet.htmlUSD = htmlUSD;

        chrome.runtime.sendMessage({
          type: MESSAGE_TYPE.GET_HTML_USD_RETURN,
          htmlUSD,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }
}
